<?php

class Mysql_Save extends Mysql_Obj {
	
	public function __construct() {
		
	}
	
	public static function saveEntryItem($item) {
		
		$title = $item->getTitle();
		$entry = $item->getEntry();
		$tags = $item->getTags();
		$user = $item->getUser();
		
		$success = true;
		
		$mysql = "INSERT INTO entry_item (title, entry, member) VALUES (\"$title\",\"$entry\",\"$user\")";
		$insertReturn = parent::runInsertQuery($mysql);
		
		if(!$insertReturn->getValid()) {
			$success = false;
		}
		
		$lastInsertId = $insertReturn->getInsertId();
		
		$mysql = "INSERT INTO tags (entryItemId, tag) VALUES ($lastInsertId, ";
		
		foreach($tags as $tag) {
			$insertReturn = parent::runInsertQuery($mysql . "\"$tag\")");
			if(!$insertReturn->getValid()) {
				$success = false;
			}
		}
		
		return $success;
		
	}
	
}