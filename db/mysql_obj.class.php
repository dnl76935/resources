<?php 

abstract class Mysql_Obj {
	
	protected $servername = "localhost";
	protected $user = "root";
	protected $pass = "dGbjna115yay";
	protected $dbname = "test";
	
	protected $mysqlConn = null;
	
	public function __construct($user = "root", $pass = "dGbjna115yay", $dbname = "test") {
		
		$this->user = $user;
		$this->pass = $pass;
		$this->dbname = $dbname;
		
		$this->mysqlConn = new mysqli($this->servername, $this->user, $this->pass, $this->dbname);
		
		if($this->mysqlConn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
	}
	
	public static function runInsertQuery($mysql) {
		
		$conn = new mysqli("localhost", "root", "dGbjna115yay", "knowledge_graph");
	
		if($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		$isValid = $conn->query($mysql);
		
		$res = new Insert_Return_Obj($isValid, $conn->insert_id);
		
		$conn->close();
		
		return $res;
	
	}
	
	public static function runSelectQuery($mysql) {
	
		$conn = new mysqli("localhost", "root", "dGbjna115yay", "knowledge_graph");
	
		if($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
	
		$results = $conn->query($mysql);
		$results = $results->fetch_all(MYSQLI_ASSOC);
		
		$conn->close();
		
		return $results;
	
	}
	
}
