<?php

class Insert_Return_Obj {
	
	protected $isValid;
	protected $insertId;
	
	public function __construct($isValid, $insertId) {
		$this->isValid = $isValid;
		$this->insertId = $insertId;
	}
	
	public function getValid() {
		return $this->isValid;
	}
	
	public function getInsertId() {
		return $this->insertId;
	}
	
}