<?php
/**
 * Created by PhpStorm.
 * User: dj
 * Date: 7/14/18
 * Time: 4:55 PM
 */

// this is used for lists and defaults to an unordered list
class Html_List extends Html_Element {

    // easy construct
    public function __construct($tag = "ul", $properties = []) {
        parent::__construct($tag, $properties);
    }

    // easy toString
    public function __toString() {
        return parent::__toString();
    }

    // adds li element
    public function addItem($value) {
        $this->text .= new Html_Element("li", ["text" => $value]);
    }

}