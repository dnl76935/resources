<?php 

class Node {
	
	public $id = "";
	public $connectedNodes = [];
	public $description = "";
	
	public function __construct($id, $connectedNodes = [], $description = "") {
		$this->id = $id;
		$this->connectedNodes = $connectedNodes;
		$this->description = $description;
	}
	
	public function __toString() {
		
		$retStr = "MY ID: $this->id<br>MY Description: $this->description<br>";
		
		$retStr .= "Connected Nodes<br>";
		
		foreach($this->connectedNodes as $node) {
			$retStr .= "ID: $node->id<br>Description: $node->description<br><br>";
		}
		
		return $retStr;
		
	}
	
	public function addConnection(Node $node) {
		$this->connectedNodes[] = $node;
		$node->connectedNodes[] = $this;
	}
	
	//Possible Problem
	public function removeConnection(Node $node) {
		for($i = 0; $i < count($this->connectedNodes); $i++) {
			if(strcmp($this->connectedNodes[$i]->id, $node->id) == 0) {
				array_splice($this->connectedNodes, $i, 1);
				$node->removeConnection($this);
			}
		}
	}
	
}

class Graph {
	
	public $nodes = [];
	
	public function __construct($nodes = []) {
		$this->nodes = $nodes;
	}
	
	public function __toString() {
		
		$retStr = "";
		
		foreach($this->nodes as $node) {
			$retStr .= $node."<br>";
		}
		
		return $retStr;
		
	}
	
	public function addNode(Node $node) {
		$this->connectedNodes[] = $node;
	}
	
	public function removeNode(Node $node) {
		for($i = 0; $i < count($this->nodes); $i++) {
			if(strcmp($this->nodes[$i]->id, $node->id) == 0) {
				array_splice($this->nodes, $i, 1);
			}
		}
	}
	
}

?>