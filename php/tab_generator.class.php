<?php 

class Tab_Generator {
	
	// list of tabs
	protected $tabs = [];
	
	// can take in some preset tabs
	public function __construct($tabs = ['Home' => 'Nothing is going on.']) {
		foreach($tabs as $key => $value) {
			$this->tabs[$key] = $value;
		}
	}
	
	// put everything together
	public function buildTabStrip() {
		
		$retStr = $this->buildTabs() . $this->buildInfoSections();
		
		return $retStr;
		
	}
	
	// build the tabs
	// these display above the info sections and are the ones you click on
	protected function buildTabs() {
		
		// create the container and button
		$container = new Html_Element("div");
		$container->class = " tab ";
		$container->text = "";
		$button = new Html_Element("button");
		
		// customize each button
		foreach($this->tabs as $cat => $value) {
			$button->class = "tablinks";
			$button->id = $cat;
			$button->onclick = "openTab(event, this)";
			$button->text = $cat;
			
			$container->text .= $button;
		}
		
		return $container;
		
	}
	
	// build the info sections
	// this goes below the tabs, it contains the important stuff you will see
	// this will also preset the first tab in the tabs array as the default visible tab
	protected function buildInfoSections() {
		
		$retStr = "";
		$container = new Html_Element("div");
		
		$count = 0;
		
		// go through the values in the array and
		// build the sections
		foreach($this->tabs as $cat => $value) {
			$container->id = $cat;
			$container->text = "<h3>$cat</h3>";
			$container->text .= $value;
			if($count == 0) {
				$container->class = " visible tabContent ";
				$count++;
			} else {
				$container->class = " tabContent ";
			}
			
			$retStr .= $container;
		}
		
		return $retStr;
		
	}
	
}

?>