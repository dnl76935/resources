<?php

class Graph_Container {
	
	
	
	public function __construct() {
		
		
		
	}
	
	public function getContainer() {
		
		$container = new Html_Element("div");
		
		$container->id = "cy";
		$container->class = "container";
		
		$container->text .= $this->getBuildButton();
		
		return $container;
		
	}
	
	protected function getBuildButton() {
		
		$button = new Html_Element("button");
		
		$button->onclick = "buildGraph()";
		$button->text .= "Click me to build graph";
		
		return $button;
		
	}
	
}