<?php

class Cytoscape_Edge {

	protected $id = "";
	protected $source = "";
	protected $target = "";

	public function __construct($id, $source, $target) {
		$this->id = $id;
		$this->source = $source;
		$this->target = $target;
	}

	public function getData() {

		$edge = array();

		$edge["id"] = $this->id;
		$edge["source"] = $this->source;
		$edge["target"] = $this->target;

		return $edge;

	}

}