<?php

class Header_Generator {
	
	public $title = "";
	
	public function __construct($title) {
		$this->title = $title;
	}
	
	// builds the header
	public function buildHeader() {
		
		$banner = new Html_Element("div");
		$banner->text = $this->title;
		$banner->class = " banner ";
		
		return $banner;
		
	}
	
}

?>