<?php

class Cytoscape_Elements {
	
	protected $network;
	
	public function __construct($network = array()) {
		$this->network = $network;
	}
	
	public function addNodeOrEdge($ele) {
		$this->network[] = $ele;
	}
	
	public function getNetwork() {
		return $this->network;
	}
	
}