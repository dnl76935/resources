<?php

abstract class Shell_V0 {

	abstract protected function getBody();
	
	public function __construct() {

	}

	// returns html for page
	public function getHtml($title) {
		return "<!DOCTYPE html> <html>" . $this->getHead($title) . $this->getBody() . "</html>";
	}
	
	// get the head for the page
	protected function getHead($title) {
	
		$head = new Html_Element("head");
	
		// make and set the title
		$titleEl = new Html_Element("title");
		$titleEl->text = $title;
		
		$head->text = $titleEl;
	
		// include all the css files
		foreach(scandir(getcwd()."/css") as $cssFile) {
			if(strcmp($cssFile, ".") != 0 && strcmp($cssFile, "..") != 0) {
				$head->text .= $this->cssLink("css/$cssFile");
			}
		}
	
		// include jquery
		$head->text .= $this->jsLink("http://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.js");
	
		// include all the js files
		foreach(scandir(getcwd()."/js") as $jsFile) {
			if(strcmp($jsFile, ".") != 0 && strcmp($jsFile, "..") != 0) {
				$head->text .= $this->jsLink("js/$jsFile");
			}
		}
	
		return $head;
	
	}
	
	// easy way to add css link
	protected function cssLink($href) {
	
		$link = new Html_Element("link");
	
		$link->rel = "stylesheet";
		$link->type = "text/css";
		$link->href = $href;
	
		return $link;
	
	}
	
	// easy way to add js link
	protected function jsLink($src) {
	
		$script = new Html_Element("script");
	
		$script->src = $src;
	
		return $script;
	
	}

}

?>