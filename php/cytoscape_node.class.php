<?php

class Cytoscape_Node {
	
	protected $id = "";
	
	public function __construct($id) {
		$this->id = $id;
	}
	
	public function getData() {
		
		$node = array();
		
		$node["id"] = $this->id;
		
		return $node;
		
	}
	
}