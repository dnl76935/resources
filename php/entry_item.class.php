<?php

class Entry_Item {
	
	protected $title;
	protected $entry;
	protected $tags;
	protected $user;
	
	public function __construct($title = "", $entry = "", $tags = array(), $user = "") {
		$this->title = $title;
		$this->entry = $entry;
		$this->tags = $tags;
		$this->user = $user;
	}
	
	public function __toString() {
		
		$ret = "The title of this entry item is: title - $this->title and entry - $this->entry with tags: ";
		
		foreach($this->tags as $tag) {
			$ret .= $tag . " | ";
		}
		
		return $ret;
		
	}
	
	public function save() {
		
		Mysql_Save::saveEntryItem($this);
		
	}
	
	public static function getById($id) {
		
		$mysql = "SELECT title, entry FROM entry_item WHERE id = " . $id;
		
		$results = Mysql_Obj::runSelectQuery($mysql);
		
		$title = $results[0]["title"];
		$entry = $results[0]["entry"];
		$tags = array();
		
		$mysql = "SELECT tag FROM tags WHERE entryItemId = " . $id;
		
		$results = Mysql_Obj::runSelectQuery($mysql);
		
		foreach($results as $result) {
			$tags[] = $result["tag"];
		}
		
		$entryItem = new Entry_Item($title, $entry, $tags);
		
		return $entryItem;
		
	}
	
	public function getTitle() {
		return $this->title;
	}
	
	public function getEntry() {
		return $this->entry;
	}
	
	public function getTags() {
		return $this->tags;
	}
	
	public function getUser() {
		return $this->user;
	}
	
	public function getTagCount() {
		return sizeof($this->tags);
	}
	
	
	
}