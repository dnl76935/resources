<?php

class Html_Element {

	protected $tag = "";
	protected $properties = [];
	protected $propertiesNoEndTag = ["link"];

	public function __construct($tag = "p", $properties = []) {
		$this->tag = $tag;
		
		foreach($properties as $key => $value) {
			$this->$key = $value;
		}
	}

	// send it to a string
	public function __toString() {

		$retStr = "<$this->tag";

		$text = "";

		// assumed key for the text in an element is text
		foreach($this->properties as $key => $value) {
			if(strcmp($key, "text") == 0 && $this->isPropSet("text")) {
				$text = $value;
			} else if(strcmp($key, "tag") != 0) {
				$retStr .= " $key = \"$value\" ";
			}
		}
		
		// check to see if element needs ending tag
		if(in_array($this->tag, $this->propertiesNoEndTag)) {
			$retStr .= ">";
		} else {
			 $retStr .= ">" . $text . "</$this->tag>";
		}

		return $retStr;

	}
	
	// magic methods
	public function __get($property) {
		if(isset($this->properties[$property])) {
			return $this->properties[$property];
		}
		return "";
	}

	public function __set($property, $value) {
		$this->properties[$property] = $value;
	}
	
	// true if property has already been set
	// will still return true if property has been set to null
	// this only checks for existence of the key matching property
	public function isPropSet($property) {
		$keys = array_keys($this->properties);
		return in_array($property, $keys);
	}

}